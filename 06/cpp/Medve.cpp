/*
 * Medve.cpp
 *
 *  Created on: Apr 24, 2015
 *      Author: bukulin
 */
#include <iostream>
#include "Medve.h"

void Medve::Hang()
{
	Allat::Hang();
	std::cout << "Brumm-brumm\n";
}

Medve::Medve()
:Allat(6),fulekSzama(2)
{
	std::cout << "Medve konstruktor\n";
}
Medve::~Medve()
{
	std::cout << "Medve destruktor\n";
}
