/*
 * Medve.h
 *
 *  Created on: Apr 24, 2015
 *      Author: bukulin
 */

#ifndef MEDVE_H_
#define MEDVE_H_
#include "Allat.h"

class Medve: public Allat{

public:
	Medve();
	~Medve();
	void Hang();

private:
	const int fulekSzama;
};

#endif /* MEDVE_H_ */
