/*
 * Allat.h
 *
 *  Created on: Apr 24, 2015
 *      Author: bukulin
 */

#ifndef ALLAT_H_
#define ALLAT_H_

class Allat
{

protected:
	Allat(int fogakSzama);
public:
	virtual ~Allat();
	virtual void Hang()=0;

protected:
	const int fogakSzama;

};

#endif /* ALLAT_H_ */
