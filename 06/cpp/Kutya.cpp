/*
 * Kutya.cpp
 *
 *  Created on: Apr 24, 2015
 *      Author: bukulin
 */

#include "Kutya.h"
#include <iostream>

void Kutya::Hang()
{
	std::cout << "Vau-vau\n";
}

Kutya::Kutya()
:Allat(4)
{
	std::cout << "Kutya konstruktor\n";
}
Kutya::~Kutya()
{
	std::cout << "Kutya destruktor\n";
}
