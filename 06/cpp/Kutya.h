/*
 * Kutya.h
 *
 *  Created on: Apr 24, 2015
 *      Author: bukulin
 */

#ifndef KUTYA_H_
#define KUTYA_H_

#include"Allat.h"

class Kutya:public Allat{

public:
	Kutya();
	~Kutya();
	void Hang();

};

#endif /* KUTYA_H_ */
