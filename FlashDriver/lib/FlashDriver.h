#ifndef __FLASH_DRIVER_H__
#define __FLASH_DRIVER_H__ 1

#include "IO/IO.h"

enum FlashErrors
{
	FLASH_OK = 0,
	FLASH_UNKNOWN_PROGRAM_ERROR = -1,
	FLASH_VPP_ERROR = -2,
	FLASH_PROGRAM_ERROR = -3,
	FLASH_PROTECTED_BLOCK_ERROR = -4,
	FLASH_READ_BACK_ERROR = -5,
	FLASH_TIMEOUT_ERROR = -6
};

int Flash_Write(IoAddress address, IoData data);

#endif
