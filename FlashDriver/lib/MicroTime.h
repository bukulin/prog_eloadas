/*
 * MicroTime.h
 *
 *  Created on: Dec 17, 2015
 *      Author: bukulin
 */

#ifndef MICROTIME_H_
#define MICROTIME_H_

#include <stdint.h>

uint32_t MicroTime_Get();

#endif /* MICROTIME_H_ */
