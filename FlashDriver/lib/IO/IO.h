/*
 * IO.h
 *
 *  Created on: Nov 12, 2015
 *      Author: bukulin
 */

#ifndef IO_H_
#define IO_H_
#include <stdint.h>

typedef uint32_t IoAddress;
typedef uint16_t IoData;

IoData IO_Read(IoAddress addr);
void IO_Write(IoAddress addr, IoData data);

#endif /* IO_H_ */
