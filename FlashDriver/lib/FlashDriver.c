#include "FlashDriver.h"

enum Registers
	{
		COMMAND_REGISTER = 0x0,
		STATUS_REGISTER = 0x0
	};

enum Commands
	{
		PROGRAM_COMMAND = 0x40,
		RESET = 0xFF
	};

union FlashStatus
{
	IoData data;
	struct
	{
		int gap0:1;
		int protectedBlock:1;
		int gap1:1;
		int vppError:1;
		int programError:1;
		int gap2:2;
		int ready:1;
	} bits;

	struct
	{
		int gap0:1;
		int errors:4;
		int gap2:2;
		int ready:1;
	} errorBits;
};

enum
{
	READY = 1 << 7,
	FLASH_WRITE_TIMEOUT_IN_MICROSECONDS = 5000
};

static int WriteError(const union FlashStatus status)
{
	if(status.errorBits.errors)
		IO_Write(COMMAND_REGISTER,RESET);

	if(status.bits.vppError)
		return FLASH_VPP_ERROR;

	if(status.bits.programError)
		return FLASH_PROGRAM_ERROR;

	if(status.bits.protectedBlock)
		return FLASH_PROTECTED_BLOCK_ERROR;

	return FLASH_UNKNOWN_PROGRAM_ERROR;
}

int Flash_Write(IoAddress address, IoData data)
{
	union FlashStatus status;
	const uint32_t expireTime = MicroTime_Get() + FLASH_WRITE_TIMEOUT_IN_MICROSECONDS;

	IO_Write(COMMAND_REGISTER,PROGRAM_COMMAND);
	IO_Write(address, data);

	do
	{
		status.data = IO_Read(STATUS_REGISTER);
		if (MicroTime_Get() >= expireTime)
		{
			return FLASH_TIMEOUT_ERROR;
		}

	} while (!status.bits.ready);

	if(status.data != READY)
		return WriteError(status);

	if (IO_Read(address)!= data)
		return FLASH_READ_BACK_ERROR;

	return FLASH_OK;
}
