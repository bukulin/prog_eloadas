/*
 * FakeMicroTime.h
 *
 *  Created on: Dec 17, 2015
 *      Author: bukulin
 */

#ifndef FAKEMICROTIME_H_
#define FAKEMICROTIME_H_

#include "MicroTime.h"
#include <stdint.h>

void FakeMicroTime_Init(uint32_t start, uint32_t increment);

#endif /* FAKEMICROTIME_H_ */
