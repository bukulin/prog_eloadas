/*
 * FakeMicroTime.c
 *
 *  Created on: Dec 17, 2015
 *      Author: bukulin
 */

#include "FakeMicroTime.h"

static uint32_t time;
static uint32_t increment;

void FakeMicroTime_Init(uint32_t start, uint32_t _increment)
{
	time = start;
	increment = _increment;
}

uint32_t MicroTime_Get()
{
	const uint32_t originalTime = time;

	time += increment;
	return originalTime;
}
