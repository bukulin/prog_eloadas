/*
 * MockIOCppUtest.c
 *
 *  Created on: Dec 17, 2015
 *      Author: bukulin
 */

#include "MockIOCppUtest.h"
#include <CppUTestExt/MockSupport_c.h>

IoData IO_Read(IoAddress addr)
{
	return mock_c()->
			actualCall(__FUNCTION__)->
			withIntParameters("addr", addr)->
			returnValue().value.unsignedIntValue;
}

void IO_Write(IoAddress addr, IoData data)
{
	mock_c()->
			actualCall(__FUNCTION__)->
			withIntParameters("addr", addr)->
			withIntParameters("data", data);
}

