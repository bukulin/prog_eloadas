/*
 * MockIO.h
 *
 *  Created on: Nov 12, 2015
 *      Author: bukulin
 */

#ifndef MOCKIO_H_
#define MOCKIO_H_
#include "IO/IO.h"
#include <stddef.h>
#include <stdbool.h>

void MockIO_Create(const size_t maxExpectations);
void MockIO_Destroy();
void MockIO_Expect_Write(IoAddress addr, IoData data);
void MockIO_Expect_ReadThenReturn(IoAddress addr, IoData data);
bool MockIO_Verify_Complete();

#endif /* MOCKIO_H_ */
