/*
 * MockIO.c
 *
 *  Created on: Nov 12, 2015
 *      Author: bukulin
 */
#include "mocks/MockIO.h"
#include <string.h>
#include <stdio.h>
#include <stdbool.h>

enum {
	NoExpectedValue
};

enum IoKind
{
	IoWrite,
	IoRead
};
struct Expectation
{
	enum IoKind kind;
	IoAddress addr;
	IoData data;
};


typedef struct Expectation Expectation;

static Expectation* expectations;
static size_t maxExpectationCount;
static size_t setExpectationCount;
static size_t getExpectationCount;

static IoAddress actualAddr;
static IoData actualData;
static bool failureAlreadyReported;

typedef void (*reporter)(void);

static void ReportTooManyWriteExpectations()
{
	fprintf(stderr,"No more room for write expectation\n");
	fprintf(stderr,"Max expectation: %u\n",maxExpectationCount);
}

static void ReportTooManyReadExpectations()
{
	fprintf(stderr,"No more room for read expectation\n");
	fprintf(stderr,"Max expectation: %u\n",maxExpectationCount);
}

static void ReportWriteButOutOfExpectations()
{
	fprintf(stderr,"Unexpected call to Io_Write\n");
	fprintf(stderr,"Actual address: %u ,actual data: %u\n",actualAddr,actualData);
}

static void ReportExpectReadWasWrite()
{
	fprintf(stderr,"Expected call to Io_Read but Io_Write happened\n");
	fprintf(stderr,"Actual address: %u ,actual data: %u\n",actualAddr,actualData);
	fprintf(stderr,"Expected address: %u \n",expectations[getExpectationCount].addr);
}

static void ReportWriteDoesNotMatch()
{
	fprintf(stderr,"Actual parameters to Io_Write don't meet expected\n");
	fprintf(stderr,"Actual address: %u ,actual data: %u\n",actualAddr,actualData);
	fprintf(stderr,"Expected address: %u ,expected data: %u\n",
			expectations[getExpectationCount].addr,expectations[getExpectationCount].data);
}

static void ReportReadButOutOfExpectations()
{
	fprintf(stderr,"Unexpected call to Io_Read\n");
	fprintf(stderr,"Actual address: %u \n",actualAddr);
}

static void ReportExpectWriteWasRead()
{
	fprintf(stderr,"Expected call to Io_Write but Io_Read happened\n");
	fprintf(stderr,"Actual address: %u\n",actualAddr);
	fprintf(stderr,"Expected address: %u, expected data: %u \n",
			expectations[getExpectationCount].addr, expectations[getExpectationCount].data);
}

static void ReportReadAddressDoesNotMatch()
{
	fprintf(stderr,"Actual parameter to Io_Read doesn't meet expected\n");
	fprintf(stderr,"Actual address: %u, expected address: %u\n",
			actualAddr,	expectations[getExpectationCount].addr);
}

static void ReportUnusedExpectations()
{
	fprintf(stderr,"Nem az történt, amit akartál, te hülye.\n");
	fprintf(stderr,"Expected %u calls, but was %u\n",
			setExpectationCount, getExpectationCount);
}

void MockIO_Create(const size_t maxExpectations)
{
	expectations = calloc(maxExpectations, sizeof(Expectation));
	maxExpectationCount = maxExpectations;
	setExpectationCount=0;
	getExpectationCount=0;
	failureAlreadyReported = false;
}
void MockIO_Destroy()
{
	if (expectations != NULL)
		free(expectations);

	expectations = NULL;
}

static void RecordExpectation(const enum IoKind kind,const IoAddress addr,const IoData data) {
	expectations[setExpectationCount].kind = kind;
	expectations[setExpectationCount].addr = addr;
	expectations[setExpectationCount].data = data;
	setExpectationCount++;
}


static bool FailWhen(bool condition, reporter report,const char* file, const int line)
{
	if (failureAlreadyReported)
		return true;

	if(!condition)
	{
		return true;
	}
	fprintf(stderr,"%s:%d: ",file,line);
	if (report != NULL)
		report();
	failureAlreadyReported = true;
	return false;
}

#define FailWhenNoRoomForExpectations(report) \
		FailWhen((setExpectationCount >= maxExpectationCount),report,__FILE__,__LINE__)

#define FailWhenNotInitialized() \
		FailWhen((expectations == NULL),NULL,__FILE__,__LINE__)

#define FailWhenNoUnusedExpectations(report) \
		FailWhen((setExpectationCount <= getExpectationCount),report,__FILE__,__LINE__)

#define FailWhenNotAllExpectationsUsed(report) \
		FailWhen((getExpectationCount != setExpectationCount),report,__FILE__,__LINE__)

void MockIO_Expect_Write(IoAddress addr, IoData data)
{
	FailWhenNoRoomForExpectations(ReportTooManyWriteExpectations);
	RecordExpectation(IoWrite,addr, data);
}

void MockIO_Expect_ReadThenReturn(IoAddress addr, IoData data)
{
	FailWhenNoRoomForExpectations(ReportTooManyReadExpectations);
	RecordExpectation(IoRead,addr, data);
}

bool MockIO_Verify_Complete()
{
	return FailWhenNotAllExpectationsUsed(ReportUnusedExpectations);
}

static void SetExpectedAndActual(IoAddress addr, IoData data)
{
	actualAddr = addr;
	actualData = data;
}

static bool ExpectationIsNot(enum IoKind kind)
{
	return (expectations[getExpectationCount].kind != kind);
}

static bool ExpectedAddressIsNot(IoAddress addr)
{
	return (expectations[getExpectationCount].addr != addr);
}

static bool ExpectedDataIsNot(IoData data)
{
	return (expectations[getExpectationCount].data != data);
}

void IO_Write(IoAddress addr, IoData data)
{
	SetExpectedAndActual(addr,data);
	FailWhenNotInitialized();
	FailWhenNoUnusedExpectations(ReportWriteButOutOfExpectations);
	FailWhen(ExpectationIsNot(IoWrite),ReportExpectReadWasWrite,__FILE__,__LINE__);
	FailWhen(ExpectedAddressIsNot(addr),ReportWriteDoesNotMatch,__FILE__,__LINE__);
	FailWhen(ExpectedDataIsNot(data),ReportWriteDoesNotMatch,__FILE__,__LINE__);
	getExpectationCount++;
}

IoData IO_Read(IoAddress addr)
{
	SetExpectedAndActual(addr,NoExpectedValue);
	FailWhenNotInitialized();
	FailWhenNoUnusedExpectations(ReportReadButOutOfExpectations);
	FailWhen(ExpectationIsNot(IoRead),ReportExpectWriteWasRead,__FILE__,__LINE__);
	FailWhen(ExpectedAddressIsNot(addr),ReportReadAddressDoesNotMatch,__FILE__,__LINE__);
	return expectations[getExpectationCount++].data;
}
