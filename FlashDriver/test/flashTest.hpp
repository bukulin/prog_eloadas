/*
 * flashTest.hpp
 *
 *  Created on: Nov 26, 2015
 *      Author: bukulin
 */

#ifndef FLASHTEST_HPP_
#define FLASHTEST_HPP_

extern "C" {
#include "FlashDriver.h"
#include "mocks/MockIO.h"
#include "FakeMicroTime.h"
}
#include <CppUTest/TestHarness.h>
#include <CppUTestExt/MockSupport.h>
#include <type_traits>

TEST_GROUP(Flash)
{
	TEST_SETUP()
	{
		status.data = 0;
	}

	TEST_TEARDOWN()
	{
		CHECK_TRUE(MockIO_Verify_Complete());
	}

	union FlashStatus
	{
		IoData data;
		struct
		{
			unsigned int gap0:1;
			unsigned int protectedBlock:1;
			unsigned int gap1:1;
			unsigned int vppError:1;
			unsigned int programError:1;
			unsigned int gap2:2;
			unsigned int ready:1;
		} bits;
	};

	enum Registers
	{
		COMMAND_REGISTER = 0x0,
		STATUS_REGISTER = 0x0
	};

	enum Commands
	{
		PROGRAM_COMMAND = 0x40,
		RESET = 0xFF
	};

protected:
	FlashStatus status;

	template<typename Enumerated>
	constexpr typename std::underlying_type<Enumerated>::type ToUType(Enumerated e)
	{
		return static_cast<typename std::underlying_type<Enumerated>::type>(e);
	}

	void MockIO_Expect_Write(IoAddress addr, IoData data)
	{
		mock().
			expectOneCall("IO_Write").
			withParameter("addr", addr).
			withParameter("data", data);
	}

	void MockIO_Expect_ReadThenReturn(IoAddress addr, IoData data)
	{
		mock().
				expectOneCall("IO_Read").
				withParameter("addr", addr).
				andReturnValue(data);

	}

	bool MockIO_Verify_Complete()
	{
		mock().checkExpectations();
		mock().clear();
		return true;
	}

};

TEST(Flash, WriteSucceeds_NotReadyImmediately)
{
	MockIO_Expect_Write(COMMAND_REGISTER, PROGRAM_COMMAND);
	MockIO_Expect_Write(0x3000, 0xBEEF);
	status.bits.ready = 0;
	MockIO_Expect_ReadThenReturn(STATUS_REGISTER, status.data);
	MockIO_Expect_ReadThenReturn(STATUS_REGISTER, status.data);
	status.bits.ready = 1;
	MockIO_Expect_ReadThenReturn(STATUS_REGISTER, status.data);
	MockIO_Expect_ReadThenReturn(0x3000, 0xBEEF);
	const auto ret = Flash_Write(0x3000, 0xBEEF);
	CHECK_EQUAL(FLASH_OK, ret);
}

TEST(Flash, WriteSucceeds_ReadyImmediately)
{
	MockIO_Expect_Write(COMMAND_REGISTER, PROGRAM_COMMAND);
	MockIO_Expect_Write(0x1000, 0xBEEF);
	status.bits.ready = 1;
	MockIO_Expect_ReadThenReturn(STATUS_REGISTER, status.data);
	MockIO_Expect_ReadThenReturn(0x1000, 0xBEEF);
	const auto ret = Flash_Write(0x1000, 0xBEEF);
	CHECK_EQUAL(FLASH_OK, ret);
}

TEST(Flash, InvalidProgrammingVoltage)
{
	MockIO_Expect_Write(COMMAND_REGISTER, PROGRAM_COMMAND);
	MockIO_Expect_Write(0x2000, 0xB00B);
	status.bits.ready = 1;
	status.bits.vppError = 1;
	MockIO_Expect_ReadThenReturn(STATUS_REGISTER, status.data);
	MockIO_Expect_Write(COMMAND_REGISTER, RESET);
	const auto ret = Flash_Write(0x2000, 0xB00B);
	CHECK_EQUAL(FLASH_VPP_ERROR, ret);
}

TEST(Flash, ProgramError)
{
	MockIO_Expect_Write(COMMAND_REGISTER, PROGRAM_COMMAND);
	MockIO_Expect_Write(0x2000, 0xB00B);
	status.bits.ready = 1;
	status.bits.programError = 1;
	MockIO_Expect_ReadThenReturn(STATUS_REGISTER, status.data);
	MockIO_Expect_Write(COMMAND_REGISTER, RESET);
	const auto ret = Flash_Write(0x2000, 0xB00B);
	CHECK_EQUAL(FLASH_PROGRAM_ERROR, ret);
}

TEST(Flash, ProtectedBlockError)
{
	MockIO_Expect_Write(COMMAND_REGISTER, PROGRAM_COMMAND);
	MockIO_Expect_Write(0x2000, 0xB00B);
	status.bits.ready = 1;
	status.bits.protectedBlock = 1;
	MockIO_Expect_ReadThenReturn(STATUS_REGISTER, status.data);
	MockIO_Expect_Write(COMMAND_REGISTER, RESET);
	const auto ret = Flash_Write(0x2000, 0xB00B);
	CHECK_EQUAL(FLASH_PROTECTED_BLOCK_ERROR, ret);
}

TEST(Flash, FlashReadBackError)
{
	MockIO_Expect_Write(COMMAND_REGISTER, PROGRAM_COMMAND);
	MockIO_Expect_Write(0x2000, 0xBEEF);
	status.bits.ready = 1;
	MockIO_Expect_ReadThenReturn(STATUS_REGISTER, status.data);
	MockIO_Expect_ReadThenReturn(0x2000, 0xBAAF);
	const auto ret = Flash_Write(0x2000, 0xBEEF);
	CHECK_EQUAL(FLASH_READ_BACK_ERROR, ret);
}

TEST(Flash, UnknownProgramError)
{
	MockIO_Expect_Write(COMMAND_REGISTER, PROGRAM_COMMAND);
	MockIO_Expect_Write(0x2000, 0xB00B);
	status.bits.ready = 1;
	status.bits.gap1 = 1;
	MockIO_Expect_ReadThenReturn(STATUS_REGISTER, status.data);
	MockIO_Expect_Write(COMMAND_REGISTER, RESET);
	const auto ret = Flash_Write(0x2000, 0xB00B);
	CHECK_EQUAL(FLASH_UNKNOWN_PROGRAM_ERROR, ret);
}

TEST(Flash, WriteSucceeds_IgnoreOtherBitsUntilReady)
{
	MockIO_Expect_Write(COMMAND_REGISTER, PROGRAM_COMMAND);
	MockIO_Expect_Write(0x3000, 0xBEEF);
	status.bits.ready = 0;
	MockIO_Expect_ReadThenReturn(STATUS_REGISTER, status.data);
	status.bits.protectedBlock = 1;
	MockIO_Expect_ReadThenReturn(STATUS_REGISTER, status.data);
	status.bits.ready = 1;
	status.bits.protectedBlock = 0;
	MockIO_Expect_ReadThenReturn(STATUS_REGISTER, status.data);
	MockIO_Expect_ReadThenReturn(0x3000, 0xBEEF);
	const auto ret = Flash_Write(0x3000, 0xBEEF);
	CHECK_EQUAL(FLASH_OK, ret);
}

TEST(Flash, WriteFails_Timeout)
{
	FakeMicroTime_Init(0, 500);
	MockIO_Expect_Write(COMMAND_REGISTER, PROGRAM_COMMAND);
	MockIO_Expect_Write(0x3000, 0xBEEF);
	status.bits.ready = 0;
	status.bits.protectedBlock = 1;
	for (int i=0; i<10; ++i)
	{
		MockIO_Expect_ReadThenReturn(STATUS_REGISTER, status.data);
	}
	const auto ret = Flash_Write(0x3000, 0xBEEF);
	CHECK_EQUAL(FLASH_TIMEOUT_ERROR, ret);
}

TEST(Flash, WriteFails_TimeoutAtTheEndOfTime)
{
	FakeMicroTime_Init(0xFFFFFFFF, 500);
	MockIO_Expect_Write(COMMAND_REGISTER, PROGRAM_COMMAND);
	MockIO_Expect_Write(0x3000, 0xBEEF);
	status.bits.ready = 0;
	status.bits.protectedBlock = 1;
	for (int i=0; i<10; ++i)
	{
		MockIO_Expect_ReadThenReturn(STATUS_REGISTER, status.data);
	}
	const auto ret = Flash_Write(0x3000, 0xBEEF);
	CHECK_EQUAL(FLASH_TIMEOUT_ERROR, ret);
}

#endif /* FLASHTEST_HPP_ */
