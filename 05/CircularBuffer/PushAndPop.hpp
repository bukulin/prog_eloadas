#ifndef PUSHANDPOP_HPP
#define PUSHANDPOP_HPP 1

#include "TestBase.hpp"

extern "C"
{
#include "CircularBuffer.h"
}

#include <CppUTest/TestHarness.h>
#include <vector>

using namespace std;

TEST_GROUP_BASE(PushAndPop,TestBase)
{
	TEST_SETUP()
	{
		CircularBuffer_Create(4);
	}

	TEST_TEARDOWN()
	{
		CircularBuffer_Destroy();
	}

	void CheckPop(const int ExpectedRetval, const int ExpectedPopValue=0)
	{
		int PoppedValue;
		const int RetVal = CircularBuffer_Pop(&PoppedValue);
		CHECK_EQUAL(ExpectedRetval, RetVal);
		if (RetVal == 0)
			CHECK_EQUAL(ExpectedPopValue,PoppedValue);
	}

	void PopMany(const vector<int>& Numbers)
	{
		for (auto i: Numbers)
			CheckPop(0,i);
	}
};

TEST(PushAndPop,PushAfterDestroyIsInvalid)
{
	CircularBuffer_Destroy();
	const int res = CircularBuffer_Push(3);
	CHECK_EQUAL(-1, res);
}

TEST(PushAndPop,PopAfterDestroyIsInvalid)
{
	CircularBuffer_Push(3);
	CircularBuffer_Destroy();
	CheckPop(-1);
}

TEST(PushAndPop, PushAnElement)
{
	const int res = CircularBuffer_Push(3);
	CHECK_EQUAL(0, res);
}

TEST(PushAndPop, PopOneElement)
{
	CircularBuffer_Push(3);

	int poppedValue = 432;
	const int res = CircularBuffer_Pop(&poppedValue);
	CHECK_EQUAL(0, res);
	CHECK_EQUAL(3, poppedValue);
}

TEST(PushAndPop, PopIntoNullDoesNotCrash)
{
	CircularBuffer_Push(3);
	const int res = CircularBuffer_Pop(nullptr);
	CHECK_EQUAL(-1, res);
}


TEST(PushAndPop,PushtoFullIsInvalid)
{
	PushMany({3,5,7,11});
	const int RetVal = CircularBuffer_Push(13);
	CHECK_EQUAL(-1, RetVal);
}

TEST(PushAndPop, PopFromEmptyIsInvalid)
{
	CheckPop(-1);
}

TEST(PushAndPop, PushManyPopMany)
{
	PushMany({1,2,4,8});
	PopMany({1,2,4,8});
}

TEST(PushAndPop, PushTooManyPopMany)
{
	PushMany({1,2,4,8});
	CircularBuffer_Push(24);
	PopMany({1,2,4,8});
}

TEST(PushAndPop, TurnAround)
{
	PushMany({1,2,3,4});
	PopMany({1,2,3,4});
	CHECK_EQUAL(0,CircularBuffer_Push(17));
	CheckPop(0,17);
}
#endif
