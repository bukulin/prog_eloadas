/*
 * TestBase.cpp
 *
 *  Created on: Apr 17, 2015
 *      Author: bukulin
 */

#include "TestBase.hpp"
extern "C"
{
	#include "CircularBuffer.h"
}
#include <CppUTest/TestHarness.h>


using namespace std;

TestBase::~TestBase() {
	// TODO Auto-generated destructor stub
}
TestBase::TestBase():Utest() {
	// TODO Auto-generated constructor stub

}

void TestBase::PushMany(const vector<int>& Numbers)
{
	for (auto i: Numbers)
	{
		const int RetVal = CircularBuffer_Push(i);
		CHECK_EQUAL(0, RetVal);
	}
}


