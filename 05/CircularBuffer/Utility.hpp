#ifndef UTILITY_HPP
#define UTILITY_HPP 1

#include "TestBase.hpp"

extern "C"
{
#include "CircularBuffer.h"
}

#include <CppUTest/TestHarness.h>

TEST_GROUP_BASE(Utility, TestBase)
{
	static const size_t BufferSize=10;
	TEST_SETUP()
	{
		CircularBuffer_Create(BufferSize);
	}

	TEST_TEARDOWN()
	{
		CircularBuffer_Destroy();
	}

	void FillCircularBuffer(const size_t size)
	{
		vector <int> v(size, 1);
		PushMany(v);
	}
};

TEST(Utility, NewlyCreatedIsEmpty)
{
	CHECK_TRUE(CircularBuffer_Empty());
}

TEST(Utility, NewlyCreatedIsNotFull)
{
	CHECK_FALSE(CircularBuffer_Full());
}

TEST(Utility, EmptyBufferIsEmpty)
{
	CircularBuffer_Push(23);
	int poppedValue;
	CircularBuffer_Pop(&poppedValue);
	CHECK_TRUE(CircularBuffer_Empty());
}

TEST(Utility, AfterOnePushBufferIsNotEmpty)
{
	CircularBuffer_Push(23);
	CHECK_FALSE(CircularBuffer_Empty());
}

TEST(Utility, FullBufferIsFull)
{
	FillCircularBuffer(BufferSize);
	CHECK_TRUE(CircularBuffer_Full());
	CircularBuffer_Destroy();
	CircularBuffer_Create(6);
	FillCircularBuffer(6);
	CHECK_TRUE(CircularBuffer_Full());
}

TEST(Utility, EmptyBufferIsNotFull)
{
	CHECK_FALSE(CircularBuffer_Full());
}

TEST(Utility, AlmostFullIsNotFull)
{
	FillCircularBuffer(BufferSize);
	int poppedValue;
	CircularBuffer_Pop(&poppedValue);
	CHECK_FALSE(CircularBuffer_Full());
}

#endif
