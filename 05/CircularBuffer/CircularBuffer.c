#include "CircularBuffer.h"
#include <stdlib.h>

static int * ItsBuffer;
static int ItsPushIndex;
static int ItsPopIndex;
static size_t ItsUsed;
static size_t ItsSize;

static bool IsCreated()
{
	return (ItsBuffer!=NULL);
}

int CircularBuffer_Create(const size_t numberOfElements)
{
	if (numberOfElements == 0)
		return -1;

	if (IsCreated())
		CircularBuffer_Destroy();

	ItsBuffer = calloc(numberOfElements,sizeof(int));
	if (ItsBuffer == NULL) return -1;
	ItsPushIndex = 0;
	ItsPopIndex = 0;
	ItsUsed = 0;
	ItsSize = numberOfElements;
	return 0;
}

void CircularBuffer_Destroy()
{
	free(ItsBuffer);
	ItsBuffer = NULL;
}

static int StepIndex(int index)
{
	index++;
	if (index == ItsSize)
		return 0;

	return index;
}


int CircularBuffer_Push(const int element)
{
	if(!IsCreated())
		return -1;
	if(CircularBuffer_Full())
		return -1;
	ItsBuffer[ItsPushIndex] = element;
	ItsPushIndex = StepIndex(ItsPushIndex);
	ItsUsed++;
	return 0;
}

int CircularBuffer_Pop(int* element)
{
	if(!IsCreated())
		return -1;
	if (element == NULL)
		return -1;
	if(CircularBuffer_Empty())
		return -1;
	*element = ItsBuffer[ItsPopIndex];
	ItsPopIndex = StepIndex(ItsPopIndex);
	ItsUsed--;
	return 0;
}

bool CircularBuffer_Empty()
{
	return (ItsUsed==0);
}

bool CircularBuffer_Full()
{
	return (ItsUsed==ItsSize);
}
