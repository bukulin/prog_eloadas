/*
 * TestBase.hpp
 *
 *  Created on: Apr 17, 2015
 *      Author: bukulin
 */

#ifndef TESTBASE_HPP_
#define TESTBASE_HPP_

#include <CppUTest/Utest.h>
#include <vector>

class TestBase: public Utest {
public:
	virtual ~TestBase();

protected:
	void PushMany(const std::vector<int>& Numbers);
	TestBase();
};

#endif /* TESTBASE_HPP_ */
