/*
 * LightDriver.h
 *
 *  Created on: Feb 4, 2016
 *      Author: bukulin
 */

#ifndef LIGHTDRIVER_H_
#define LIGHTDRIVER_H_

struct LightDriverStruct;

typedef struct LightDriverStruct* LightDriver;

void LightDriver_TurnOn(LightDriver self);
void LightDriver_TurnOff(LightDriver self);
void LightDriver_Destroy(LightDriver self);

#endif /* LIGHTDRIVER_H_ */
