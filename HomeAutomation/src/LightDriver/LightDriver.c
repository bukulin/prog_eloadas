/*
 * LightDriver.c
 *
 *  Created on: Feb 18, 2016
 *      Author: bukulin
 */
#include "LightDriver/LightDriverPrivate.h"
#include "LightDriver/LightDriver.h"
#include <stddef.h>
#include <stdbool.h>

static bool IsValid(LightDriver self)
{
	return self && self->vtable;
}

void LightDriver_TurnOn(LightDriver self)
{
	if (IsValid(self) && (self->vtable->TurnOn != NULL))
		return self->vtable->TurnOn(self);
}

void LightDriver_TurnOff(LightDriver self)
{
	if (IsValid(self) && (self->vtable->TurnOff != NULL))
			return self->vtable->TurnOff(self);
}

void LightDriver_Destroy(LightDriver self)
{
	if (IsValid(self) && (self->vtable->Destroy != NULL))
			return self->vtable->Destroy(self);
}
