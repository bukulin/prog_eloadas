/*
 * X10LightDriver.h
 *
 *  Created on: Feb 4, 2016
 *      Author: bukulin
 */

#ifndef X10LIGHTDRIVER_H_
#define X10LIGHTDRIVER_H_

#include "LightDriver/LightDriver.h"

enum X10_HouseCode{
	X10_A,
	X10_B,
	X10_C,
	X10_D,
	X10_E,
	X10_F,
	X10_G,
	X10_H,
	X10_I,
	X10_J,
	X10_K,
	X10_L,
	X10_M,
	X10_N,
	X10_O,
	X10_P
};

typedef enum X10_HouseCode X10_HouseCode;

LightDriver X10LightDriver_Create(int id, X10_HouseCode house, int unit);

#endif /* X10LIGHTDRIVER_H_ */
