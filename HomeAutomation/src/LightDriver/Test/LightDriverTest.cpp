/*
 * LightDriverTest.cpp
 *
 *  Created on: Feb 18, 2016
 *      Author: bukulin
 */

#include <CppUTest/CommandLineTestRunner.h>
#include <CppUTest/TestHarness.h>

extern "C"
{
#include "LightDriver/LightDriverPrivate.h"
#include "LightDriver/LightDriver.h"
}

static void ShouldNotBeCalled(LightDriver)
{
	FAIL("Bukta");
}

TEST_GROUP(LightDriver)
{
	LightDriverStruct lightDriver;
	LightDriverInterfaceStruct interface;

	TEST_SETUP()
	{
		interface = LightDriverInterfaceStruct{
					.Destroy = ShouldNotBeCalled,
					.TurnOn = ShouldNotBeCalled,
					.TurnOff = ShouldNotBeCalled
			};

		lightDriver.vtable = &interface;
	}

	TEST_TEARDOWN()
	{

	}
};


TEST(LightDriver, NullInterfaceDoesNotCrash)
{

	lightDriver.vtable = nullptr;
	LightDriver_TurnOn(&lightDriver);
	LightDriver_TurnOff(&lightDriver);
	LightDriver_Destroy(&lightDriver);
}

TEST(LightDriver, NullFunctionPointersDoesNotCrash)
{

	interface.Destroy = nullptr;
	interface.TurnOn = nullptr;
	interface.TurnOff = nullptr;

	LightDriver_TurnOn(&lightDriver);
	LightDriver_TurnOff(&lightDriver);
	LightDriver_Destroy(&lightDriver);
}

TEST(LightDriver, NullDriverDoesNotCrash)
{
	LightDriver_TurnOn(nullptr);
	LightDriver_TurnOff(nullptr);
	LightDriver_Destroy(nullptr);
}

int main(const int argc, const char *argv[])
{
	return CommandLineTestRunner::RunAllTests(argc, argv);
}
