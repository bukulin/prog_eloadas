/*
 * LightDriver.hpp
 *
 *  Created on: Mar 24, 2016
 *      Author: bukulin
 */

#ifndef LIGHTDRIVER_HPP_
#define LIGHTDRIVER_HPP_


class LightDriver
{
protected:
	LightDriver(const int aId);

public:
	virtual ~LightDriver();

	virtual void TurnOn() = 0;
	virtual void TurnOff() = 0;

protected:
	int id;
};


#endif /* LIGHTDRIVER_HPP_ */
