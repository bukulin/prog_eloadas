/*
 * X10LightDriver.cpp
 *
 *  Created on: Mar 24, 2016
 *      Author: bukulin
 */


#include "X10LightDriver.hpp"

X10LightDriver::X10LightDriver(const int aId)
	: LightDriver(aId)
{}

void X10LightDriver::TurnOn()
{
	//...
}

void X10LightDriver::TurnOff()
{
	//...
}
