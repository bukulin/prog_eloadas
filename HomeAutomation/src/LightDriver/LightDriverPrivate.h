/*
 * LightDriverPrivate.h
 *
 *  Created on: Feb 11, 2016
 *      Author: bukulin
 */

#ifndef LIGHTDRIVERPRIVATE_H_
#define LIGHTDRIVERPRIVATE_H_

struct LightDriverInterfaceStruct {
	void (*Destroy)(struct LightDriverStruct* self);
	void (*TurnOn)(struct LightDriverStruct* self);
	void (*TurnOff)(struct LightDriverStruct* self);
};
typedef struct LightDriverInterfaceStruct LightDriverInterfaceStruct;
typedef LightDriverInterfaceStruct* LightDriverInterface;

struct LightDriverStruct {
	LightDriverInterface vtable;
	int id;
};
typedef struct LightDriverStruct LightDriverStruct;

#endif /* LIGHTDRIVERPRIVATE_H_ */
