/*
 * X10LightDriver.c
 *
 *  Created on: Feb 4, 2016
 *      Author: bukulin
 */
#include "LightDriver/X10LightDriver.h"
#include "LightDriver/LightDriverPrivate.h"
#include "LightController/LightController.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

enum {
	MAX_X10_MESSAGE_LENGTH = 64
};

struct X10LightDriverStruct {
	LightDriverStruct base;
	X10_HouseCode house;
	int unit;
	char message[MAX_X10_MESSAGE_LENGTH];
};

typedef struct X10LightDriverStruct X10LightDriverStruct;

typedef X10LightDriverStruct* X10LightDriver;

static void X10LightDriver_Destroy(LightDriver base){
	X10LightDriver self = (X10LightDriver)base;
	free(self);
}

static void formatTurnOnMessage(X10LightDriver self){
	snprintf(self->message, MAX_X10_MESSAGE_LENGTH, "On: %d, %d", self->house, self->unit);
}

static void formatTurnOffMessage(X10LightDriver self){
	snprintf(self->message, MAX_X10_MESSAGE_LENGTH, "Off: %d, %d", self->house, self->unit);
}

static void sendMessage(X10LightDriver self){
}

static void X10LightDriver_TurnOn(LightDriver base){
	X10LightDriver self = (X10LightDriver)base;

	formatTurnOnMessage(self);
	sendMessage(self);
}

static void X10LightDriver_TurnOff(LightDriver base){
	X10LightDriver self = (X10LightDriver)base;

	formatTurnOffMessage(self);
	sendMessage(self);
}

static const LightDriverInterfaceStruct interface={
		.Destroy = X10LightDriver_Destroy,
		.TurnOn = X10LightDriver_TurnOn,
		.TurnOff = X10LightDriver_TurnOff};

LightDriver X10LightDriver_Create(int id, X10_HouseCode house, int unit)
{
	X10LightDriver self = calloc(1, sizeof(*self));
	if (self == NULL)
		return NULL;

	self->base.vtable = &interface;
	self->base.id = id;
	self->house = house;
	self->unit = unit;

	return (LightDriver)self;
}

