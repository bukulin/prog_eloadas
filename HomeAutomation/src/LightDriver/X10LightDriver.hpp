/*
 * X10LightDriver.hpp
 *
 *  Created on: Mar 24, 2016
 *      Author: bukulin
 */

#ifndef X10LIGHTDRIVER_HPP_
#define X10LIGHTDRIVER_HPP_

#include "LightDriver.hpp"

class X10LightDriver final: public LightDriver
{
public:
	X10LightDriver(const int aId);

public:
	void TurnOn() override;
	void TurnOff() override;
};

#endif /* X10LIGHTDRIVER_HPP_ */
