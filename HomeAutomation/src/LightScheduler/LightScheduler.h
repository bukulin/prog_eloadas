/*
 * LightScheduler.h
 *
 *  Created on: Jun 18, 2015
 *      Author: bukulin
 */

#ifndef LIGHTSCHEDULER_H_
#define LIGHTSCHEDULER_H_

typedef enum Day{
	MONDAY=0,
	TUESDAY,
	WEDNESDAY,
	THURSDAY,
	FRIDAY,
	SATURDAY,
	SUNDAY,

	EVERYDAY=10,
	WEEKDAY,
	WEEKEND,
	NONE=-1
} Day;

enum LsResult {
	LS_OK = 0,
	LS_TOO_MANY_EVENTS,
	LS_ID_OUTOFBOUNDS
};

void LightScheduler_Create(void);
void LightScheduler_Destroy(void);

void LightScheduler_WakeUp(void);
int LightScheduler_ScheduleTurnOn(const int lightId, const Day day, const int minute);
int LightScheduler_ScheduleTurnOff(const int lightId, const Day day, const int minute);
void LightScheduler_ScheduleRemove(const int lightId, const Day day, const int minute);
void LightScheduler_Randomize(const int lightId, const Day day, const int minute);

#endif /* LIGHTSCHEDULER_H_ */
