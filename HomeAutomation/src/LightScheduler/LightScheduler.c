/*
 * LightScheduler.c
 *
 *  Created on: Jun 18, 2015
 *      Author: bukulin
 */
#include "LightScheduler.h"
#include "TimeService/TimeService.h"
#include "LightController/LightController.h"
#include "RandomMinute/RandomMinute.h"
#include <stdbool.h>
#include <stddef.h>

enum{
	UNUSED = -1,TURNON,TURNOFF,MAXEVENTS = 128
};

typedef struct
{
	int id;
	int minuteOfDay;
	Day day;
	int event;
} ScheduledLightEvent;

static ScheduledLightEvent scheduledEvents[MAXEVENTS];

void LightScheduler_Create(void)
{
	int i;

	for(i=0;i<MAXEVENTS;++i)
		scheduledEvents[i].id = UNUSED;

	TimeService_SetPeriodicAlarmInSeconds(60, LightScheduler_WakeUp);
}

void LightScheduler_Destroy()
{
	TimeService_CancelPeriodicAlarmInSeconds(60, LightScheduler_WakeUp);
}

static void OperateLight(const ScheduledLightEvent *lightEvent)
{
	if (lightEvent->event == TURNON)
		LightController_On(lightEvent->id);
	else
		LightController_Off(lightEvent->id);
}

static bool DoesLightRespondToday(const Time *time, const ScheduledLightEvent *lightEvent){

	const int reactionDay = lightEvent->day;
	const int today = time->dayOfWeek;

	if (reactionDay==EVERYDAY)
		return true;

	if(reactionDay==today)
		return true;

	if(reactionDay==WEEKEND && (today == SATURDAY || today == SUNDAY))
		return true;

	if (reactionDay==WEEKDAY && (today >= MONDAY) && (today <= FRIDAY))
		return true;

	return false;
}

static void ProcessEventDueNow(const Time *time, const ScheduledLightEvent *lightEvent)
{
	if(lightEvent->id==UNUSED)
		return;

	if(lightEvent->minuteOfDay!=time->minuteOfDay)
		return;

	if (!DoesLightRespondToday(time, lightEvent))
		return;

	OperateLight(lightEvent);
}

void LightScheduler_WakeUp(void)
{
	Time time;
	int i;

	TimeService_GetTime(&time);

	for(i=0;i<MAXEVENTS;++i)
	{
		ProcessEventDueNow(&time, &scheduledEvents[i]);
	}
}

static int ScheduleEvent(const int lightId, const int minute, const Day day, const int state)
{
	int i;

	if ((lightId < 0) || (lightId > 31))
		return LS_ID_OUTOFBOUNDS;

	for(i=0;i<MAXEVENTS;++i)
	{
		if (scheduledEvents[i].id == UNUSED)
		{
			scheduledEvents[i].id = lightId;
			scheduledEvents[i].minuteOfDay = minute;
			scheduledEvents[i].day = day;
			scheduledEvents[i].event = state;

			return LS_OK;
		}
	}
	return LS_TOO_MANY_EVENTS;
}

int LightScheduler_ScheduleTurnOn(const int lightId, const Day day, const int minute)
{
	return ScheduleEvent(lightId, minute, day, TURNON);
}

int LightScheduler_ScheduleTurnOff(const int lightId, const Day day, const int minute)
{
	return ScheduleEvent(lightId, minute, day, TURNOFF);
}

static ScheduledLightEvent* FindScheduledEvent(const int lightId, const Day day, const int minute)
{
	int i;

	for (i = 0; i < MAXEVENTS; ++i)
		if (scheduledEvents[i].id == lightId &&
			scheduledEvents[i].minuteOfDay == minute &&
			scheduledEvents[i].day == day)
		{
			return &scheduledEvents[i];
		}

	return NULL;
}
void LightScheduler_ScheduleRemove(const int lightId, const Day day, const int minute)
{
	ScheduledLightEvent* event = FindScheduledEvent(lightId, day, minute);
	if (event == NULL)
		return;
	event->id = UNUSED;
}

void LightScheduler_Randomize(const int lightId, const Day day, const int minute)
{
	ScheduledLightEvent* event = FindScheduledEvent(lightId, day, minute);
	if (event == NULL)
		return;
	event->minuteOfDay += RandomMinute_Get();
}
