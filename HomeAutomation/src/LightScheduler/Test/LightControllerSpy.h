/*
 * LightControllerSpy.h
 *
 *  Created on: May 28, 2015
 *      Author: bukulin
 */

#ifndef LIGHTCONTROLLERSPY_H_
#define LIGHTCONTROLLERSPY_H_

#include "LightController/LightController.h"

enum {
	LIGHT_ID_UNKNOWN = -1,
	LIGHT_STATE_UNKNOWN = -1,
	LIGHT_ON = 1,
	LIGHT_OFF = 0
};

int LightControllerSpy_GetLastId();
int LightControllerSpy_GetLastState();
int LightControllerSpy_GetLightState(int id);


#endif /* LIGHTCONTROLLERSPY_H_ */
