/*
 * LightControllerSpyTest.hpp
 *
 *  Created on: May 28, 2015
 *      Author: bukulin
 */

#ifndef LIGHTCONTROLLERSPYTEST_HPP_
#define LIGHTCONTROLLERSPYTEST_HPP_

#include <CppUTest/TestHarness.h>
extern "C" {
#include "LightControllerSpy.h"
}

TEST_GROUP(LightControllerSpy)
{
	TEST_SETUP(){
		LightController_Create();
	}

	TEST_TEARDOWN(){
		LightController_Destroy();
	}
};

TEST(LightControllerSpy, Create){

	CHECK_EQUAL(LIGHT_ID_UNKNOWN,LightControllerSpy_GetLastId());
	CHECK_EQUAL(LIGHT_STATE_UNKNOWN,LightControllerSpy_GetLastState());
	CHECK_EQUAL(LIGHT_STATE_UNKNOWN,LightControllerSpy_GetLightState(3));

}

TEST(LightControllerSpy, RememberTheLastLightIdControlled)
{
	LightController_On(1);
	CHECK_EQUAL(1,LightControllerSpy_GetLastId());
	CHECK_EQUAL(LIGHT_ON, LightControllerSpy_GetLastState());
	LightController_On(2);
	CHECK_EQUAL(2,LightControllerSpy_GetLastId());

	LightController_Off(1);
	CHECK_EQUAL(1,LightControllerSpy_GetLastId());
	CHECK_EQUAL(LIGHT_OFF, LightControllerSpy_GetLastState());
}

TEST(LightControllerSpy, RememberAllLightStates)
{
	LightController_On(3);
	LightController_Off(4);
	CHECK_EQUAL(LIGHT_ON, LightControllerSpy_GetLightState(3));
	CHECK_EQUAL(LIGHT_OFF, LightControllerSpy_GetLightState(4));
}

#endif /* LIGHTCONTROLLERSPYTEST_HPP_ */
