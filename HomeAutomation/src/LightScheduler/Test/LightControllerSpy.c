/*
 * LightControllerSpy.c
 *
 *  Created on: May 28, 2015
 *      Author: bukulin
 */

#include "LightControllerSpy.h"

enum {
	NUMBER_OF_LIGHTS = 32
};
static int lastId;
static int lastState;
static int lightStates[NUMBER_OF_LIGHTS];

int LightController_Create()
{
	int i;

	lastId=LIGHT_ID_UNKNOWN;
	lastState=LIGHT_STATE_UNKNOWN;
	for (i = 0; i < NUMBER_OF_LIGHTS; i++)
		lightStates[i] = LIGHT_STATE_UNKNOWN;

	return 0;
}

void LightController_Destroy()
{

}

int LightControllerSpy_GetLastId()
{
	return lastId;
}

int LightControllerSpy_GetLastState()
{
	return lastState;
}

int LightControllerSpy_GetLightState(int id)
{
	return lightStates[id];
}

void LightController_On(int id)
{
	lastId=id;
	lastState=LIGHT_ON;
	lightStates[id] = LIGHT_ON;

}

void LightController_Off(int id)
{
	lastId=id;
	lastState=LIGHT_OFF;
	lightStates[id] = LIGHT_OFF;

}

