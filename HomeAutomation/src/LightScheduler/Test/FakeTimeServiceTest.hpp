/*
 * FakeTimeServiceTest.hpp
 *
 *  Created on: Jun 11, 2015
 *      Author: bukulin
 */

#ifndef FAKETIMESERVICETEST_HPP_
#define FAKETIMESERVICETEST_HPP_

#include <CppUTest/TestHarness.h>
extern "C" {
#include "FakeTimeService.h"
}

TEST_GROUP(FakeTimeService)
{
	TEST_SETUP()
	{
		TimeService_Create();
	}

	void CheckPeriodicAlarm(int period, WakeUpCallback callback)
	{
		POINTERS_EQUAL(callback, FakeTimeService_GetAlarmCallback());
		CHECK_EQUAL(period, FakeTimeService_GetAlarmPeriod());
	}

};

TEST(FakeTimeService, Create)
{
	Time time;

	TimeService_GetTime(&time);
	CHECK_EQUAL(TIME_UNKNOWN,time.dayOfWeek);
	CHECK_EQUAL(TIME_UNKNOWN,time.minuteOfDay);
}

TEST(FakeTimeService, Set)
{
	Time time;

	FakeTimeService_SetMinute(678);
	FakeTimeService_SetDay(MONDAY);

	TimeService_GetTime(&time);
	CHECK_EQUAL(MONDAY,time.dayOfWeek);
	CHECK_EQUAL(678,time.minuteOfDay);
}

void Dummy()
{
}

TEST(FakeTimeService, ResetPrivateVariablesOnCreate)
{
	CheckPeriodicAlarm(PERIOD_UNKNOWN, (WakeUpCallback)CALLBACK_UNKNOWN);

	TimeService_SetPeriodicAlarmInSeconds(60, Dummy);

	TimeService_Create();
	CheckPeriodicAlarm(PERIOD_UNKNOWN, (WakeUpCallback)CALLBACK_UNKNOWN);
}

TEST(FakeTimeService, CancelPeriodicAlarm)
{
	TimeService_SetPeriodicAlarmInSeconds(60, Dummy);
	TimeService_CancelPeriodicAlarmInSeconds(61, Dummy);
	CheckPeriodicAlarm(60, Dummy);
	TimeService_CancelPeriodicAlarmInSeconds(60, Dummy);
	CheckPeriodicAlarm(PERIOD_UNKNOWN, (WakeUpCallback)CALLBACK_UNKNOWN);
}

#endif /* FAKETIMESERVICETEST_HPP_ */
