/*
 * FakeTimeService.c
 *
 *  Created on: Jun 11, 2015
 *      Author: bukulin
 */
#include "FakeTimeService.h"
#include <stddef.h>

static Time fakeTime;
static WakeUpCallback actualCallBack;
static int callBackPeriod;

void TimeService_GetTime(Time *time){
	*time = fakeTime;
}

void TimeService_Create()
{
	fakeTime.dayOfWeek = TIME_UNKNOWN;
	fakeTime.minuteOfDay = TIME_UNKNOWN;
	callBackPeriod = PERIOD_UNKNOWN;
	actualCallBack = CALLBACK_UNKNOWN;
}

void FakeTimeService_SetMinute(int minute)
{
	fakeTime.minuteOfDay = minute;
}

void FakeTimeService_SetDay(int weekday)
{
	fakeTime.dayOfWeek = weekday;
}

WakeUpCallback FakeTimeService_GetAlarmCallback()
{
	return actualCallBack;
}

int FakeTimeService_GetAlarmPeriod()
{
	return callBackPeriod;
}

void TimeService_SetPeriodicAlarmInSeconds(int seconds, WakeUpCallback callback){

	actualCallBack = callback;
	callBackPeriod = seconds;
}

void TimeService_CancelPeriodicAlarmInSeconds(int seconds, WakeUpCallback callback)
{
	if (actualCallBack == callback && callBackPeriod == seconds)
	{
		actualCallBack = CALLBACK_UNKNOWN;
		callBackPeriod = PERIOD_UNKNOWN;
	}

}
