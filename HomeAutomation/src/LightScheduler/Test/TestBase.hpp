/*
 * TestBase.hpp
 *
 *  Created on: Nov 5, 2015
 *      Author: bukulin
 */

#ifndef TESTBASE_HPP_
#define TESTBASE_HPP_

#include <CppUTest/Utest.h>

class TestBase: public Utest {
public:
	TestBase();
	virtual ~TestBase();

protected:
	void SetTimeTo(int day,int minute);
	void CheckLightState(int id, int lightState);
};

#endif /* TESTBASE_HPP_ */
