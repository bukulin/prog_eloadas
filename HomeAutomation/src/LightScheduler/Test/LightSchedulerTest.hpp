/*
 * LightSchedulerTest.hpp
 *
 *  Created on: May 28, 2015
 *      Author: bukulin
 */

#ifndef LIGHTSCHEDULERTEST_HPP_
#define LIGHTSCHEDULERTEST_HPP_

#include <CppUTest/TestHarness.h>
#include "TestBase.hpp"
extern "C"
{
#include "FakeTimeService.h"
#include "LightScheduler/LightScheduler.h"
#include "LightControllerSpy.h"
}

TEST_GROUP_BASE(LightScheduler, TestBase)
{
	TEST_SETUP()
	{
		LightController_Create();
		LightScheduler_Create();
	}

};

TEST(LightScheduler,NoChangeToLightsDuringInitialization)
{
	CheckLightState(LIGHT_ID_UNKNOWN, LIGHT_STATE_UNKNOWN);
}

TEST(LightScheduler, NoScheduleNothingHappens)
{
	SetTimeTo(MONDAY,100);
	LightScheduler_WakeUp();
	CheckLightState(LIGHT_ID_UNKNOWN, LIGHT_STATE_UNKNOWN);
}


TEST(LightScheduler, ScheduleOnEveryDayNotTimeYet)
{
	LightScheduler_ScheduleTurnOn(13, EVERYDAY, 145);
	SetTimeTo(MONDAY,100);
	LightScheduler_WakeUp();
	CheckLightState(LIGHT_ID_UNKNOWN, LIGHT_STATE_UNKNOWN);
}

TEST(LightScheduler, ScheduleOnEveryDayItsTime)
{
	LightScheduler_ScheduleTurnOn(13, EVERYDAY, 145);
	SetTimeTo(MONDAY,145);
	LightScheduler_WakeUp();
	CheckLightState(13, LIGHT_ON);
}

TEST(LightScheduler, ScheduleOffEveryDayItsTime)
{
	LightScheduler_ScheduleTurnOff(14, EVERYDAY, 145);
	SetTimeTo(MONDAY,145);
	LightScheduler_WakeUp();
	CheckLightState(14, LIGHT_OFF);
}

TEST(LightScheduler, ScheduleTuesdayButItIsMonday)
{
	LightScheduler_ScheduleTurnOn(13, TUESDAY, 145);
	SetTimeTo(MONDAY,145);
	LightScheduler_WakeUp();
	CheckLightState(LIGHT_ID_UNKNOWN, LIGHT_STATE_UNKNOWN);
}

TEST(LightScheduler, ScheduleTuesdayAndItIsTuesday)
{
	LightScheduler_ScheduleTurnOn(13, TUESDAY, 145);
	SetTimeTo(TUESDAY,145);
	LightScheduler_WakeUp();
	CheckLightState(13, LIGHT_ON);
}

TEST(LightScheduler, ScheduleWeekendAndItIsTuesday)
{
	LightScheduler_ScheduleTurnOn(13, WEEKEND, 145);
	SetTimeTo(TUESDAY,145);
	LightScheduler_WakeUp();
	CheckLightState(LIGHT_ID_UNKNOWN, LIGHT_STATE_UNKNOWN);
}

TEST(LightScheduler, ScheduleWeekendAndItIsSaturday)
{
	LightScheduler_ScheduleTurnOn(13, WEEKEND, 145);
	SetTimeTo(SATURDAY,145);
	LightScheduler_WakeUp();
	CheckLightState(13, LIGHT_ON);
}

TEST(LightScheduler, ScheduleWeekendAndItIsSunday)
{
	LightScheduler_ScheduleTurnOn(1, WEEKEND, 145);
	SetTimeTo(SUNDAY,145);
	LightScheduler_WakeUp();
	CheckLightState(1, LIGHT_ON);
}

TEST(LightScheduler, ScheduleWeekendAndItIsMonday)
{
	LightScheduler_ScheduleTurnOn(2, WEEKEND, 145);
	SetTimeTo(MONDAY,145);
	LightScheduler_WakeUp();
	CheckLightState(LIGHT_ID_UNKNOWN, LIGHT_STATE_UNKNOWN);
}

TEST(LightScheduler, ScheduleWeekdayAndItIsSunday)
{
	LightScheduler_ScheduleTurnOff(6, WEEKDAY, 145);
	SetTimeTo(SUNDAY,145);
	LightScheduler_WakeUp();
	CheckLightState(LIGHT_ID_UNKNOWN, LIGHT_STATE_UNKNOWN);
}

TEST(LightScheduler, ScheduleWeekdayAndItIsMonday)
{
	LightScheduler_ScheduleTurnOn(1, WEEKDAY, 145);
	SetTimeTo(MONDAY,145);
	LightScheduler_WakeUp();
	CheckLightState(1, LIGHT_ON);
}

TEST(LightScheduler, ScheduleTwoEventsAtTheSameTime)
{
	LightScheduler_ScheduleTurnOn(7, WEEKDAY, 145);
	LightScheduler_ScheduleTurnOff(13, WEEKDAY, 145);
	SetTimeTo(MONDAY,145);
	LightScheduler_WakeUp();
	CheckLightState(7, LIGHT_ON);
	CheckLightState(13, LIGHT_OFF);
}

TEST(LightScheduler, RejectTooManyEvents)
{
	for (int i=0; i<128; i++)
	{
		CHECK_EQUAL(LS_OK, LightScheduler_ScheduleTurnOn(8, WEEKDAY, 145+i));
	}
	CHECK_EQUAL(LS_TOO_MANY_EVENTS, LightScheduler_ScheduleTurnOff(8, WEEKDAY, 144));
}

TEST(LightScheduler, RemoveRecyclesScheduleSlot)
{
	for (int i=0; i<128; i++)
	{
		CHECK_EQUAL(LS_OK, LightScheduler_ScheduleTurnOn(8, WEEKDAY, 145+i));
	}
	LightScheduler_ScheduleRemove(8, WEEKDAY, 145);
	CHECK_EQUAL(LS_OK, LightScheduler_ScheduleTurnOn(8, WEEKDAY, 145));

}

TEST(LightScheduler, RemoveMultipleScheduledEvent)
{
	LightScheduler_ScheduleTurnOn(8, WEEKDAY, 145);
	LightScheduler_ScheduleTurnOn(9, WEEKDAY, 145);
	LightScheduler_ScheduleRemove(8, WEEKDAY, 145);
	SetTimeTo(MONDAY,145);
	LightScheduler_WakeUp();
	CheckLightState(9, LIGHT_ON);
	CheckLightState(8, LIGHT_STATE_UNKNOWN);
}

TEST(LightScheduler, AcceptsValidLightIds)
{
	CHECK_EQUAL(LS_OK,LightScheduler_ScheduleTurnOn(0, WEEKDAY, 145));
	CHECK_EQUAL(LS_OK,LightScheduler_ScheduleTurnOn(31, WEEKDAY, 145));
}

TEST(LightScheduler, RejectsInvalidLightIds)
{
	CHECK_EQUAL(LS_ID_OUTOFBOUNDS,LightScheduler_ScheduleTurnOn(-1, WEEKDAY, 145));
	CHECK_EQUAL(LS_ID_OUTOFBOUNDS,LightScheduler_ScheduleTurnOn(32, WEEKDAY, 145));
}

TEST(LightScheduler, RemoveNonExistentEvent)
{
	LightScheduler_ScheduleRemove(3, EVERYDAY, 444);
}


TEST_GROUP(LightSchedulerInitAndCleanup)
{
};

TEST(LightSchedulerInitAndCleanup, CreateStartsOneMinuteAlarm)
{
	LightScheduler_Create();
	POINTERS_EQUAL(LightScheduler_WakeUp, FakeTimeService_GetAlarmCallback());
	CHECK_EQUAL(60, FakeTimeService_GetAlarmPeriod());
	LightScheduler_Destroy();
}

TEST(LightSchedulerInitAndCleanup, DestroyCancellsOneMinuteAlarm)
{
	LightScheduler_Create();
	LightScheduler_Destroy();
	POINTERS_EQUAL(CALLBACK_UNKNOWN, FakeTimeService_GetAlarmCallback());
}


#endif /* LIGHTSCHEDULERTEST_HPP_ */
