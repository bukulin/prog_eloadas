/*
 * FakeRandomMinute.h
 *
 *  Created on: Nov 5, 2015
 *      Author: bukulin
 */

#ifndef FAKERANDOMMINUTE_H_
#define FAKERANDOMMINUTE_H_


int FakeRandomMinute_Get();
void FakeRandomMinute_SetFirstAndIncrement( const int startValue,
											const int increment);

#endif /* FAKERANDOMMINUTE_H_ */
