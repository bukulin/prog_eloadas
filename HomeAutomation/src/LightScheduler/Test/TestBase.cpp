/*
 * TestBase.cpp
 *
 *  Created on: Nov 5, 2015
 *      Author: bukulin
 */

#include "TestBase.hpp"
#include <CppUTest/UtestMacros.h>

extern "C"
{
#include "FakeTimeService.h"
#include "LightControllerSpy.h"
}


TestBase::TestBase()
:Utest()
{
	// TODO Auto-generated constructor stub

}

TestBase::~TestBase() {
	// TODO Auto-generated destructor stub
}

void TestBase::SetTimeTo(int day,int minute){
	FakeTimeService_SetDay(day);
	FakeTimeService_SetMinute(minute);
}

void TestBase::CheckLightState(int id, int lightState){
	if (id == LIGHT_ID_UNKNOWN)
	{
		CHECK_EQUAL(id, LightControllerSpy_GetLastId());
		CHECK_EQUAL(lightState, LightControllerSpy_GetLastState());
	}
	else
	{
		CHECK_EQUAL(lightState, LightControllerSpy_GetLightState(id));
	}
}
