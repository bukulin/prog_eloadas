/*
 * LightSchedulerRandomizeTest.hpp
 *
 *  Created on: Oct 22, 2015
 *      Author: bukulin
 */

#ifndef LIGHTSCHEDULERRANDOMIZETEST_HPP_
#define LIGHTSCHEDULERRANDOMIZETEST_HPP_

#include <CppUTest/TestHarness.h>
#include "TestBase.hpp"

extern "C"
{
#include "LightScheduler/LightScheduler.h"
#include "RandomMinute/RandomMinute.h"

#include "FakeRandomMinute.h"
#include "LightControllerSpy.h"
}

TEST_GROUP_BASE(LightSchedulerRandomize, TestBase)
{
	TEST_SETUP()
	{
		LightController_Create();
		LightScheduler_Create();

		UT_PTR_SET(RandomMinute_Get, FakeRandomMinute_Get);
	}

	TEST_TEARDOWN()
	{
		LightScheduler_Destroy();
		LightController_Destroy();
	}
};

TEST(LightSchedulerRandomize, TurnsOnEarly)
{
	FakeRandomMinute_SetFirstAndIncrement(-10,5);
	LightScheduler_ScheduleTurnOn(13, EVERYDAY, 100);
	LightScheduler_Randomize(13, EVERYDAY, 100);
	SetTimeTo(MONDAY,100-10);
	LightScheduler_WakeUp();
	CheckLightState(13, LIGHT_ON);
}

TEST(LightSchedulerRandomize, RandomizeNonExistentEvent)
{
	LightScheduler_Randomize(3, EVERYDAY, 444);
}


#endif /* LIGHTSCHEDULERRANDOMIZETEST_HPP_ */
