/*
 * FakeTimeService.h
 *
 *  Created on: Jun 11, 2015
 *      Author: bukulin
 */

#ifndef FAKETIMESERVICE_H_
#define FAKETIMESERVICE_H_

#include "TimeService/TimeService.h"

enum {
	TIME_UNKNOWN = -1,
	CALLBACK_UNKNOWN = 0,
	PERIOD_UNKNOWN = -1
};

void FakeTimeService_SetMinute(int minute);
void FakeTimeService_SetDay(int weekday);
WakeUpCallback FakeTimeService_GetAlarmCallback();
int FakeTimeService_GetAlarmPeriod();

#endif /* FAKETIMESERVICE_H_ */
