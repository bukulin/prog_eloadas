/*
 * LightSchedulerTest.cpp

 *
 *  Created on: May 28, 2015
 *      Author: bukulin
 */
#include <CppUTest/CommandLineTestRunner.h>
#include "LightSchedulerTest.hpp"
#include "LightControllerSpyTest.hpp"
#include "FakeTimeServiceTest.hpp"


int main(int argc, char* argv[])
{
	CommandLineTestRunner::RunAllTests(argc,argv);
}
