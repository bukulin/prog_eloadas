/*
 * FakeRandomMinute.c
 *
 *  Created on: Nov 5, 2015
 *      Author: bukulin
 */

#include "FakeRandomMinute.h"

static int actualValue;
static int _increment;

int FakeRandomMinute_Get()
{
	int value = actualValue;

	actualValue += _increment;
	return value;
}

void FakeRandomMinute_SetFirstAndIncrement( const int startValue,
											const int increment)
{
	actualValue = startValue;
	_increment = increment;
}

