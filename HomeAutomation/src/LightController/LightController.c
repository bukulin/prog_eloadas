/*
 * LightController.c
 *
 *  Created on: Feb 4, 2016
 *      Author: bukulin
 */

#include "LightController/LightController.h"
#include "LightDriver/X10LightDriver.h"
#include "LightController/Test/LightDriverSpy.h"

#include <string.h>
#include <stdio.h>

static LightDriver lightDrivers[MAX_LIGHTS]={NULL};

int LightController_Create()
{
	memset(lightDrivers,0,sizeof(lightDrivers));
	return 0;
}

static void destroy(LightDriver driver)
{
	if (driver == NULL)
		return;

	LightDriver_Destroy(driver);
}

void LightController_Destroy()
{
	int i;
	for(i = 0; i < MAX_LIGHTS; ++i)
	{
		LightDriver driver = lightDrivers[i];
		destroy(driver);
		lightDrivers[i] = NULL;
	}
}

void LightController_On(int id)
{
	LightDriver driver = lightDrivers[id];

	if(driver == NULL)
		return;

	LightDriver_TurnOn(driver);
}

void LightController_Off(int id)
{
	LightDriver driver = lightDrivers[id];

	if(driver == NULL)
		return;

	LightDriver_TurnOff(driver);
}

bool LightController_Add(int id, LightDriver drv)
{
	if (id >= MAX_LIGHTS)
		return false;

	destroy(lightDrivers[id]);
	lightDrivers[id] = drv;
	return true;
}
