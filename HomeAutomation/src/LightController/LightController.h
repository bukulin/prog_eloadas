/*
 * LightController.h
 *
 *  Created on: May 28, 2015
 *      Author: bukulin
 */

#ifndef LIGHTCONTROLLER_H_
#define LIGHTCONTROLLER_H_

#include <stdbool.h>
#include "LightDriver/LightDriver.h"

enum {
	MAX_LIGHTS = 32
};

int LightController_Create();
void LightController_Destroy();

void LightController_On(int id);
void LightController_Off(int id);
bool LightController_Add(int id, LightDriver d);

#endif /* LIGHTCONTROLLER_H_ */
