#ifndef LIGHTDRIVERSPY_H
#define LIGHTDRIVERSPY_H 1

#include "LightDriver/LightDriver.h"

enum {
	LIGHT_STATE_UNKNOWN = -1,
	LIGHT_ON = 1,
	LIGHT_OFF = 0,

	LIGHT_ID_UNKNOWN = -1
};

LightDriver LightDriverSpy_Create(int id);

void LightDriverSpy_Reset();
void LightDriverSpy_AddSpiesToController();
int LightDriverSpy_GetLightState(int id);
int LightDriverSpy_GetLastId(void);
int LightDriverSpy_GetLastState(void);

#endif
