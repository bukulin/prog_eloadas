/*
 * LightControllerTest.cpp
 *
 *  Created on: Feb 11, 2016
 *      Author: bukulin
 */

#include <CppUTest/CommandLineTestRunner.h>
#include <CppUTest/TestHarness.h>

extern "C"
{
#include "LightController/LightController.h"
#include "LightDriverSpy.h"
#include "CountingLightDriver.h"
}

TEST_GROUP(LightController)
{
	TEST_SETUP()
	{
		LightDriverSpy_Reset();
		LightController_Create();
		LightDriverSpy_AddSpiesToController();
	}

	TEST_TEARDOWN()
	{
		LightController_Destroy();
	}
};

TEST(LightController, TurnOn)
{
	LightController_On(1);
	CHECK_EQUAL(LIGHT_ON, LightDriverSpy_GetLightState(1));
}

TEST(LightController, TurnOff)
{
	LightController_Off(2);
	CHECK_EQUAL(LIGHT_OFF, LightDriverSpy_GetLightState(2));
}

TEST(LightController, AddingDriverDestroysPreviousAndDoesNotLeak)
{
	LightDriver spy = LightDriverSpy_Create(1);
	LightController_Add(1, spy);
	LightController_Destroy();
}

TEST(LightController, AllDriversDestroyed)
{
	for (int i = 0; i < MAX_LIGHTS; i++)
	{
		LightDriver spy = LightDriverSpy_Create(i);
		CHECK_TRUE(LightController_Add(i, spy));
	}
}

TEST(LightController, ValidIdLowerRange)
{
	LightDriver spy = LightDriverSpy_Create(0);
	CHECK_TRUE(LightController_Add(0, spy));
}

TEST(LightController, ValidIdUpperRange)
{
	LightDriver spy = LightDriverSpy_Create(MAX_LIGHTS);
	CHECK_TRUE(LightController_Add(MAX_LIGHTS-1, spy));
}

TEST(LightController, InValidIdBeyondUpperRange)
{
	LightDriver spy = LightDriverSpy_Create(MAX_LIGHTS);
	CHECK_FALSE(LightController_Add(MAX_LIGHTS, spy));
	LightDriver_Destroy(spy);
}

TEST(LightController, NonAddedLightDoesNothing)
{
	LightController_Add(1, NULL);

	LightController_On(1);
	LONGS_EQUAL(LIGHT_STATE_UNKNOWN, LightDriverSpy_GetLightState(1));

	LightController_Off(1);
	LONGS_EQUAL(LIGHT_STATE_UNKNOWN, LightDriverSpy_GetLightState(1));
}

TEST(LightController, TurnOnDifferentTypes)
{
	auto otherDriver = CountingLightDriver_Create(5);

	LightController_Add(5, otherDriver);

	LightController_On(1);
	LightController_On(5);
	LightController_Off(5);
	LONGS_EQUAL(LIGHT_ON, LightDriverSpy_GetLightState(1));
	LONGS_EQUAL(2, CountingLightDriver_GetCallCount(otherDriver));
}

int main(const int argc, const char *argv[])
{
	return CommandLineTestRunner::RunAllTests(argc, argv);
}
