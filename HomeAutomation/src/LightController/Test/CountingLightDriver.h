/*
 * CountingLightDriver.h
 *
 *  Created on: Mar 17, 2016
 *      Author: bukulin
 */

#ifndef COUNTINGLIGHTDRIVER_H_
#define COUNTINGLIGHTDRIVER_H_

#include "LightDriver/LightDriver.h"

LightDriver CountingLightDriver_Create(int id);
int CountingLightDriver_GetCallCount(LightDriver);

#endif /* COUNTINGLIGHTDRIVER_H_ */
