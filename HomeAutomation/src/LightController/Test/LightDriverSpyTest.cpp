extern "C"
{
#include "LightDriverSpy.h"
}

#include <CppUTest/TestHarness.h>
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(LightDriverSpy)
{
	LightDriver lightDriverSpy;

	void setup()
	{
		LightDriverSpy_Reset();
		lightDriverSpy = LightDriverSpy_Create(1);
	}

	void teardown()
	{
		LightDriver_Destroy(lightDriverSpy);
	}
};

TEST(LightDriverSpy, Create)
{
	LONGS_EQUAL(LIGHT_STATE_UNKNOWN, LightDriverSpy_GetLightState(1));
}

TEST(LightDriverSpy, On)
{
	LightDriver_TurnOn(lightDriverSpy);
	LONGS_EQUAL(LIGHT_ON, LightDriverSpy_GetLightState(1));
}

TEST(LightDriverSpy, Off)
{
	LightDriver_TurnOff(lightDriverSpy);
	LONGS_EQUAL(LIGHT_OFF, LightDriverSpy_GetLightState(1));
}

TEST(LightDriverSpy, RecordsLastIdLastTurnOn)
{
	LightDriver_TurnOff(lightDriverSpy);
	LONGS_EQUAL(1, LightDriverSpy_GetLastId());
	LONGS_EQUAL(LIGHT_OFF, LightDriverSpy_GetLastState());
}

TEST(LightDriverSpy, RecordsLastIdLastTurnOff)
{
	LightDriver_TurnOn(lightDriverSpy);
	LONGS_EQUAL(1, LightDriverSpy_GetLastId());
	LONGS_EQUAL(LIGHT_ON, LightDriverSpy_GetLastState());
}

TEST_GROUP(InterfaceUninstalled)
{
	LightDriver lightDriverSpy;

	void setup()
	{
		LightDriverSpy_Reset();
		lightDriverSpy = LightDriverSpy_Create(1);
	}
};

IGNORE_TEST(InterfaceUninstalled, OperateAfterDestroyDoesNotCrash)
{
	LightDriver_Destroy(lightDriverSpy);
	LightDriver_TurnOn(lightDriverSpy);
	LONGS_EQUAL(LIGHT_STATE_UNKNOWN, LightDriverSpy_GetLastState());
}

int main(const int argc, const char* argv[])
{
	return CommandLineTestRunner::RunAllTests(argc, argv);
}
