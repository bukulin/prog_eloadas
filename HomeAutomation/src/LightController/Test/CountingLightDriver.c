/*
 * CountingLightDriver.c
 *
 *  Created on: Mar 17, 2016
 *      Author: bukulin
 */
#include <stdlib.h>
#include "LightDriver/LightDriverPrivate.h"
#include "CountingLightDriver.h"

struct CountingLightDriverStruct {
	LightDriverStruct base;
	int counter;
};

typedef struct CountingLightDriverStruct CountingLightDriverStruct;
typedef CountingLightDriverStruct* CountingLightDriver;

static void Destroy(LightDriver base)
{
	free(base);
}

static void Count(LightDriver base)
{
	CountingLightDriver self = (CountingLightDriver)base;

	self->counter++;
}

static LightDriverInterfaceStruct interface = {
		.Destroy = Destroy,
		.TurnOn = Count,
		.TurnOff = Count
};

LightDriver CountingLightDriver_Create(int id)
{
	CountingLightDriver self = calloc(1, sizeof(*self));
	if (self == NULL)
		return NULL;

	self->base.vtable = &interface;
	self->base.id = id;
	return self;
}

int CountingLightDriver_GetCallCount(LightDriver base)
{
	CountingLightDriver self = (CountingLightDriver)base;

	return self->counter;
}
