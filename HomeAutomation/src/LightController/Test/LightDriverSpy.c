#include "LightDriverSpy.h"
#include "LightDriver/LightDriverPrivate.h"
#include "LightController/LightController.h"

#include <stdlib.h>
#include <stddef.h>

static int states[MAX_LIGHTS];
static int lastState = LIGHT_STATE_UNKNOWN;
static int lastId = LIGHT_ID_UNKNOWN;

struct LightDriverSpyStruct {
	LightDriverStruct base;
};
typedef struct LightDriverSpyStruct LightDriverSpyStruct;
typedef LightDriverSpyStruct* LightDriverSpy;


static void LightDriverSpy_Destroy(LightDriver base)
{
	LightDriverSpy self = (LightDriverSpy)base;
	free(self);
}

static void save(int id, int state)
{
	states[id] = state;
	lastId = id;
	lastState = state;
}

static void LightDriverSpy_TurnOn(LightDriver base)
{
	LightDriverSpy self = (LightDriverSpy)base;
	save(self->base.id, LIGHT_ON);
}

static void LightDriverSpy_TurnOff(LightDriver base)
{
	LightDriverSpy self = (LightDriverSpy)base;
	save(self->base.id, LIGHT_OFF);
}

void LightDriverSpy_Reset()
{
	unsigned int i;
	for (i = 0; i < MAX_LIGHTS; ++i)
	{
		states[i] = LIGHT_STATE_UNKNOWN;
	}

	lastState = LIGHT_STATE_UNKNOWN;
}

void LightDriverSpy_AddSpiesToController()
{
	unsigned int i;
	for (i = 0; i < 3; ++i)
		LightController_Add(i, LightDriverSpy_Create(i));
}

int LightDriverSpy_GetLightState(int id)
{
	return states[id];
}

int LightDriverSpy_GetLastId(void)
{
	return lastId;
}

int LightDriverSpy_GetLastState(void)
{
	return lastState;
}

static const LightDriverInterfaceStruct interface={
		.Destroy = LightDriverSpy_Destroy,
		.TurnOn = LightDriverSpy_TurnOn,
		.TurnOff = LightDriverSpy_TurnOff};

LightDriver LightDriverSpy_Create(int id)
{
	LightDriverSpy self = calloc(1, sizeof(*self));
	if (self == NULL)
		return NULL;

	self->base.vtable = &interface;
	self->base.id = id;
	return (LightDriver)self;
}

