/*
 * RandomMinute.h
 *
 *  Created on: Oct 22, 2015
 *      Author: bukulin
 */

#ifndef RANDOMMINUTE_H_
#define RANDOMMINUTE_H_

extern int (*RandomMinute_Get)();

void RandomMinute_Create(int bound);
void RandomMinute_Destroy();

#endif /* RANDOMMINUTE_H_ */
