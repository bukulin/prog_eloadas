/*
 * RandomMinuteTest.hpp
 *
 *  Created on: Oct 22, 2015
 *      Author: bukulin
 */

#ifndef RANDOMMINUTETEST_HPP_
#define RANDOMMINUTETEST_HPP_

extern "C" {
#include "RandomMinute/RandomMinute.h"
}

#include <CppUTest/TestHarness.h>
#include <cstring>
#include <algorithm>
#include <numeric>
#include <array>

TEST_GROUP(RandomMinute)
{
	TEST_SETUP()
	{
		RandomMinute_Create(BOUND);
		srand(1);
	}

	TEST_TEARDOWN()
	{
		RandomMinute_Destroy();
	}

	enum {
		BOUND = 30
	};

	void AssertMinuteIsInRange(const int minute) const
	{
		if (minute < -BOUND || minute > BOUND)
			FAIL("Minute is out of bound");
	}
};

TEST(RandomMinute, GetIsInRange)
{
	for (int i=0; i<100; ++i)
	{
		auto minute = RandomMinute_Get();
		AssertMinuteIsInRange(minute);
	}
}

TEST(RandomMinute, AllValuesPossible)
{
	std::array<bool, 2*BOUND+1> hits;
	hits.fill(false);

	for (int i=0; i<3000; ++i)
	{
		auto minute = RandomMinute_Get();
		hits[minute+BOUND] = true;
	}

	CHECK_TRUE(std::accumulate(hits.cbegin(), hits.cend(),
			true,
			std::logical_and<bool>()));
}

#endif /* RANDOMMINUTETEST_HPP_ */
