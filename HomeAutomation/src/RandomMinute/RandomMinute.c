/*
 * RandomMinute.c
 *
 *  Created on: Oct 22, 2015
 *      Author: bukulin
 */

#include "RandomMinute.h"
#include <stdlib.h>

static int bound;

static int RandomMinute_GetImpl()
{
	return rand()%(2*bound+1) - bound;
}

int (*RandomMinute_Get)() = RandomMinute_GetImpl;

void RandomMinute_Create(int _bound)
{
	bound = _bound;
}

void RandomMinute_Destroy()
{
}
