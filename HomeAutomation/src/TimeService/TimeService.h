/*
 * TimeService.h
 *
 *  Created on: Jun 11, 2015
 *      Author: bukulin
 */

#ifndef TIMESERVICE_H_
#define TIMESERVICE_H_


struct Time {
	int dayOfWeek;
	int minuteOfDay;
};

typedef struct Time Time;
typedef void (* WakeUpCallback) (void);

void TimeService_Create();
void TimeService_GetTime(Time *time);
void TimeService_SetPeriodicAlarmInSeconds(int seconds, WakeUpCallback callback);
void TimeService_CancelPeriodicAlarmInSeconds(int seconds, WakeUpCallback callback);

#endif /* TIMESERVICE_H_ */
