/*
 * TimeService.c
 *
 *  Created on: Aug 27, 2015
 *      Author: bukulin
 */

#include "TimeService.h"
#include <string.h>

static Time currentTime;
static WakeUpCallback currentCallback;
static int callbackPeriod;
static int callbackCounter;


void TimeService_Create()
{
	memset(&currentTime,0,sizeof(currentTime));
}

void TimeService_Destroy()
{}

void TimeService_SetTime(const Time *time)
{
	currentTime.dayOfWeek=1;
	currentTime.minuteOfDay=100;
}

void TimeService_GetTime(Time *time)
{
	*time=currentTime;
}

void TimeService_SetPeriodicAlarmInSeconds(int seconds, WakeUpCallback callback)
{
	callbackPeriod = seconds * 10;
	callbackCounter = callbackPeriod;
	currentCallback = callback;
}

void TimeService_ISR()
{
	callbackCounter--;
	if(callbackCounter != 0)
		return;

	currentCallback();
}


