/*
 * main.cpp
 *
 *  Created on: Aug 27, 2015
 *      Author: bukulin
 */

#include "TimeServiceTest.hpp"
#include <CppUTest/CommandLineTestRunner.h>

int main(const int argc, const char* argv[])
{
	return CommandLineTestRunner::RunAllTests(argc, argv);
}


