/*
 * TimeServiceTest.hpp
 *
 *  Created on: Aug 27, 2015
 *      Author: bukulin
 */

#ifndef TIMESERVICETEST_HPP_
#define TIMESERVICETEST_HPP_

#include <CppUTest/TestHarness.h>

extern "C" {
#include "TimeService.h"
}

static bool callbackIsCalled;

static void TestCallback()
{
	callbackIsCalled = true;
}

TEST_GROUP(TestList)
{
	void CheckCurrentTime(const Time& expectedTime) const
	{
		Time gettime;
		TimeService_GetTime(&gettime);
		CHECK_EQUAL(expectedTime.dayOfWeek,gettime.dayOfWeek);
		CHECK_EQUAL(expectedTime.minuteOfDay,gettime.minuteOfDay);
	}
};

IGNORE_TEST(TestList, CallBackListIsEmptyAfterInit)
{

}

IGNORE_TEST(TestList, CanNotAddCallBackBeforeCreate)
{

}

TEST(TestList, AddOneCallBackAndRunIt)
{
	TimeService_Create();

	TimeService_SetPeriodicAlarmInSeconds(20, TestCallback);
	callbackIsCalled = false;
	for(int i = 0; i < 199; ++i)
		TimeService_ISR();

	CHECK_FALSE(callbackIsCalled);
	TimeService_ISR();
	CHECK_TRUE(callbackIsCalled);
	TimeService_Destroy();
}

IGNORE_TEST(TestList, AddOneCallbackAndRunItTwice)
{}

IGNORE_TEST(TestList, AddMoreCallBacksAndRunOne)
{

}

IGNORE_TEST(TestList, AddMoreCallBacksAndRunAll)
{

}

IGNORE_TEST(TestList, AddOneCallBackAndCancel)
{

}

IGNORE_TEST(TestList, AddMoreCallBacksAndCancelOne)
{

}

TEST(TestList, GetTimeAfterCreate)
{
	TimeService_Create();
	CheckCurrentTime({0,0});
}

TEST(TestList, SetTime)
{
	const Time time {1, 100};

	TimeService_SetTime(&time);
	CheckCurrentTime(time);
}

IGNORE_TEST(TestList, NoCallBacksAfterDestroy)
{
}


IGNORE_TEST(TestList, IsTimeElapsing)
{

}
#endif /* TIMESERVICETEST_HPP_ */
